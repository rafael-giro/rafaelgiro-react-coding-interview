function addNumber(arr, target) {
  const map = {};

  for (let i = 0; i < arr.length; i++) {
    const currentNumber = arr[i];

    if (typeof map[target - currentNumber] !== 'undefined') {
      return [i, map[target - currentNumber]];
    }

    map[currentNumber] = i;
  }

  return [-1, -1];
}
