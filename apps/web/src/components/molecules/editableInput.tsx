import { Box, Input, styled } from '@mui/material';
import { ReactNode, useState } from 'react';

type EditableInputProps = {
  children: ReactNode;
  placeholder?: string;
  onSave(newValue: string): void;
  validate?: ((value: string) => boolean)[];
};

const StyledContainer = styled(Box)`
  position: absolute;
  top: 1rem;
  left: 0;
  background-color: white;
  border: 1px solid black;
  z-index: 10;
`;

export function EditableInput({
  children,
  placeholder,
  onSave,
  validate,
}: EditableInputProps) {
  const [isOpen, setIsOpen] = useState(false);
  const [newValue, setNewValue] = useState('');
  const [error, setError] = useState(false);

  function handleSubmit() {
    if (validate) {
      for (let i = 0; i < validate.length; i++) {
        if (!validate[i](newValue)) {
          setError(true);
          return;
        }
      }
    }

    onSave(newValue);
  }

  return (
    <Box sx={{ position: 'relative' }}>
      <button onClick={() => setIsOpen((c) => !c)}>{children}</button>
      {isOpen && (
        <StyledContainer>
          <Input
            type="text"
            placeholder={placeholder}
            onChange={(e) => setNewValue(e.target.value)}
          />
          <Box justifyContent="flex-end">
            {error && 'Invalid'}
            <button onClick={handleSubmit}>Save</button>
            <button onClick={() => setIsOpen(false)}>Cancel</button>
          </Box>
        </StyledContainer>
      )}
    </Box>
  );
}
