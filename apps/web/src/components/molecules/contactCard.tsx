import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import { EditableInput } from './editableInput';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

function isEmpty(newValue: string): boolean {
  if (!newValue) return false;

  return true;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          <EditableInput onSave={console.log} validate={[isEmpty]}>
            <Typography variant="subtitle1" lineHeight="1rem">
              {name}
            </Typography>
          </EditableInput>
          <EditableInput onSave={console.log}>
            <Typography variant="caption" color="text.secondary">
              {email}
            </Typography>
          </EditableInput>
        </Box>
      </Box>
    </Card>
  );
};
